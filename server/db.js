// read config
var config = require('./config');

var Sequelize = require("sequelize");

// sqlize object 
// first param is the DB name - NOT table name
var sequelize = new Sequelize ('employees',
    config.MYSQL_USERNAME,
	config.MYSQL_PASSWORD,
	{
		host: config.MYSQL_HOSTNAME,
		port: config.MYSQL_PORT,
		logging: config.MYSQL_LOGGING,
		dialect: 'mysql',
		pool: {
			max: 5,
			min: 0,
			idle: 10000,
		},
	}
);

// import models 
// use .import() to avoid the double-loading problem with require(file)(sequelize, Sequelize);
const Employee = sequelize.import('./models/employee');



module.exports = {
  Employee: Employee,
  sequelize: sequelize
};