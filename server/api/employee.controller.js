
// db is not passed in here, so you just use it

var create = function(db) {
  return function(req, res) {
    console.log('This is the data... >>>>\n');
    console.log('>>>>' + JSON.stringify(req.body));

    db.Employee
      .create({
        emp_no: req.body.employee.emp_no,
        birth_date: req.body.employee.birth_date,
        first_name: req.body.employee.first_name,
        last_name: req.body.employee.last_name,
        gender: req.body.employee.gender,
        hire_date: req.body.employee.hire_date
      })
      .then(function(employee) {
        console.log('Success');
        res
          .status(200)
          .json(employee);
      })
      .catch(function(err) {
        console.log("error: " + err);
        res
          .status(500)
          .json(err);
      }); // end return
  }
};// create 

// the show is based on searchString
var retrieve = function(db) {
    return function(req, res) {
        console.log('This is the data... >>>>\n');
        console.log('>>>>' + JSON.stringify(req.query));
    
        var query = req.query.searchString + "%";

        // exported sequelize = Employee model
        db.Employee
            .findAll({
                where: {
                    $or: [
                    { first_name: { $like: query } },
                    { last_name: { $like: query } }
                    ]
                }// returns an array of JSON 
            }
            )
            // .query('SELECT * FROM employees WHERE first_name LIKE ?',
            //     { model: 'employee' },
            //     { replacements: [query], type: sequelize.QueryTypes.SELECT }
            // )
            .then(function(employee) {
                res.status(200).json(employee);
            })
            .catch(function(err) {
                console.log("Employee error clause: " + err);
                res.status(500).json(err);
            });
    }// end return
};// end retrive

var edit = function(db) {
    return function(req, res) {
        console.log('This is the emp_no in URL params>>>>' + JSON.stringify(req.params));
        console.log('This is the field to be changed>>>>' + JSON.stringify(req.body));
    // find record sent by URL params 
    // change field sent by req.body

    db.Employee
        .update({
            first_name: req.body.first_name,
            last_name: req.body.last_name,
        }, {
            where : {
                emp_no: req.params.emp_no
            }
        }
        )
        .then(function(employee) {
            res.status(200).json(employee);
            })
        .catch(function(err) {
            console.log("Employee error clause: " + err);
            res.status(500).json(err);
        });
    }// end return callback
}; // end edit 

var destroy = function(db) {
    return function(req, res) {
        console.log('This is the emp_no in URL params>>>>' + JSON.stringify(req.params));
    // find record by URL params, then delete
    
    db.Employee
        .destroy({
            where : {
                emp_no: req.params.emp_no
            },
            force: true
        }
        )
        .then(function(employee) {
            res.status(200).json(employee);
            })
        .catch(function(err) {
            console.log("Employee error clause: " + err);
            res.status(500).json(err);
        });
    }// end return callback to express & mysql 
}; // end delete

module.exports = function(db) {
  return {
    create: create(db),
    retrieve: retrieve(db),
    edit: edit(db),
    destroy: destroy(db)
  }
};