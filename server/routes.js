module.exports = function(app, db) {
  // instantiate the models you need to call methods from
  var Employee = require('./api/employee.controller')(db);

  // create employee
  app.post("/api/employees", Employee.create);

  // retrieve employee
  app.get("/api/employees", Employee.retrieve);

  // edit employee 
  app.put('/api/employees/:emp_no', Employee.edit);

  // delete employee
  app.delete('/api/employees/:emp_no', Employee.destroy);

};

