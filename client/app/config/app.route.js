(function(){
    // add the .config property which holds uiRouteConfig
    angular
        .module("MyApp")
        .config(uiRouteConfig)
    
    // attach $inject property 
    uiRouteConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    // uiRouteConfig 
    function uiRouteConfig($stateProvider, $urlRouterProvider){
        // .state takes 2 params - state name string & object
        // can chain
        // optional to define controller here
        $stateProvider
            .state('register', {
                url : '/register',
                templateUrl: "/app/registration/register.html" // templateUrl camelcase
            }
            )
            .state('search',{
                url : '/search',
                templateUrl: '/app/search/search.html'
            }
            );
        
        // set catchall URL
        $urlRouterProvider
            .otherwise('/search');

    }// close uiRouteConfig 


})();