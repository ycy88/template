(function (){
    angular
        .module("MyApp")
        .service("EmpService", EmpS)

    // must inject $http to talk to server
    EmpS.$inject = ["$http"]
    
    function EmpS() {
        var self = this;

        // expose functions 
        self.insertEmployee = insertEmployee;

        // insertEmployee takes one param Emp object - see registerC
        // creates a post request to our end point 
        function insertEmployee(employee) {
            return $http.post('/api/employees', {
                    employee: employee
                }// this object is req.body
            );
        }

    }// close service

})();