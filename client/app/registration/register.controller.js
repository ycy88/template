(function (){
    angular
        .module("MyApp")
        .controller("RegC", RegC)
    
    RegC.$inject = ["$window", "EmpService"]; 

    function RegC($window, EmpS) {
        var con = this;

        // init and expose 
        con.employee = {
            emp_no: null,
            birth_date: null,
            first_name: '',
            last_name: '',
            gender: null,
            hire_date: null,
        };

        con.status = {
            message: '',
            code: ''
        }
        
        con.register = register;

        function register() {
            alert('You have entered: \n' + JSON.stringify(con.employee));
            // pray it will handle the datatypes okay
            // screw $window first
            EmpS
                .insertEmployee(con.employee)
                .then(function(result){
                    console.log("The result is >>>>" + JSON.stringify(result));                                        
                })
                .catch(function(err){
                    console.log("The err is >>>>>" + JSON.stringify(err));
                    con.status.message = err.data.name;
                    con.status.code = err.data.parent.errno;
                });
        }

    }// close controller 

})();